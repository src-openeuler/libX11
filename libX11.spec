Name:		libX11
Version:	1.8.10
Release:	1
Summary:	Core X11 protocol client library
License:	MIT and X11
URL:		https://www.x.org
Source0:	https://www.x.org/releases/individual/lib/%{name}-%{version}.tar.xz

Patch1: 	dont-forward-keycode-0.patch
Patch2: 	libX11-1.7.2-sw_64.patch

BuildRequires: pkgconfig(inputproto)
BuildRequires: pkgconfig(kbproto)
BuildRequires: pkgconfig(xcb) >= 1.11.1
BuildRequires: pkgconfig(xextproto)
BuildRequires: pkgconfig(xf86bigfontproto) >= 1.2.0
BuildRequires: pkgconfig(xorg-macros) >= 1.15
BuildRequires: pkgconfig(xproto) >= 7.0.25
BuildRequires: pkgconfig(xtrans)
BuildRequires: xmlto >= 0.0.22
BuildRequires: make

Provides:       %{name}-common = %{version}-%{release}
Obsoletes:      %{name}-common < %{version}-%{release}

Provides:       %{name}-xcb = %{version}-%{release}
Obsoletes:      %{name}-xcb < %{version}-%{release}

%description
Core X11 protocol client library.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for %{name}.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
autoreconf -ivf
%if 0%{?build_cross} == 1
%configure --disable-silent-rules --enable-malloc0returnsnull
%else
%configure --disable-silent-rules
%endif
%make_build

%install
%make_install

mkdir -p $RPM_BUILD_ROOT/var/cache/libX11/compose
%delete_la

# Don't install Xcms.txt - find out why upstream still ships this.
find $RPM_BUILD_ROOT -name 'Xcms.txt' -delete

# package these properly
rm -rf $RPM_BUILD_ROOT%{_docdir}

%check
%make_build check

%files
%doc AUTHORS
%license COPYING
%{_libdir}/*.so.*
%{_datadir}/X11/locale/*

%{_datadir}/X11/XErrorDB
%dir /var/cache/libX11
%dir /var/cache/libX11/compose

%files          devel
%{_libdir}/*.a
%{_libdir}/*.so
%{_includedir}/X11/*.h
%{_libdir}/pkgconfig/*.pc
%{_includedir}/X11/extensions/XKBgeom.h

%files          help
%doc README.md ChangeLog
%{_mandir}/*/*

%changelog
* Sun Aug 04 2024 Funda Wang <fundawang@yeah.net> - 1.8.10-1
- update to 1.8.10

* Mon Feb 05 2024 zhouwenpei <zhouwenpei1@h-partners.com> - 1.8.7-2
- revent "disable static library"

* Wed Oct 04 2023 Funda Wang <fundawang@yeah.net> - 1.8.7-1
- update to 1.8.7
- merge sw64 patch

* Sat Jul 22 2023 zhouwenpei <zhouwenpei1@h-partners.com> - 1.8.6-1
- update to 1.8.6

* Mon Jun 19 2023 liweigang <liweiganga@uniontech.com> - 1.8.4-2
- fix CVE-2023-3138

* Wed Apr 12 2023 liweiganga <liweiganga@uniontech.com> - 1.8.4-1
- update to 1.8.4

* Tue Apr 11 2023 zhangpan <zhangpan103@h-partners.com> - 1.8.1-4
- add build options for i686

* Fri Nov 04 2022 wangkerong <wangkerong@h-partners.com> - 1.8.1-3
- disable static library

* Mon Oct 24 2022 zhouwenpei <zhouwenpei1@h-partners.com> - 1.8.1-2
- fix CVE-2022-3554

* Mon Jun 20 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 1.8.1-1
- update to 1.8.1

* Wed Dec 15 2021 wangkerong <wangkerong@huawei.com> - 1.7.2-2
- provide XKBgeom header file

* Sat Nov 27 2021 dongyuzhen <dongyuzhen@huawei.com> - 1.7.2-1
- update to 1.7.2

* Fri Jun 11 2021 zhanglin <lin.zhang@turbolinux.com.cn> - 1.7.0-2
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:fix CVE-2021-31535

* Thu Jan 28 2021 hanhui <hanhui15@huawei.com> - 1.7.0-1
- Type: enhancement
- ID:   NA
- SUG:  NA
- DESC: update to 1.7.0

* Wed Sep 30 2020 chengguipeng<chenguipeng1@huawei.com> - 1.6.9-4
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:fix CVE-2020-14363

* Tue Sep 8 2020 zhanghua <zhanghua40@huawei.com> - 1.6.9-3
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:fix CVE-2020-14344

* Mon Nov 4 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.6.9-2
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:remove the XKBgeom.h

* Fri Nov 1 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.6.9-1
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:update to 1.6.9

* Sat Oct 19 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.6.7-2
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:change the directory of the license file

* Fri Aug 30 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.6.7-1
- Package init
